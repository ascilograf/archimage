﻿using UnityEngine;
using System.Collections;

public class JobManager : MonoBehaviour 
{
	static JobManager _instance = null;
	public static JobManager instance
	{
		get
		{
			if( !_instance)
			{
				_instance = FindObjectOfType(typeof ( JobManager ) ) as JobManager;

				if( !_instance )
				{
					var obj = new GameObject( "JobManager" );
					_instance = obj.AddComponent<JobManager>();
				}
			}

			return _instance;
		}
	}

	void OnApplicationQuit()
	{
		_instance = null;
	}
}

public class Job
{
	public event System.Action<bool> jobComplete;

	private bool _running;
	public bool running{ get { return _running; } }

	private bool _paused;
	public bool paused{ get { return _paused; } }

	private IEnumerator _coroutine;
	private bool _jobWasKilled;
	//private Stack<Job> _childJobs;

	public Job( IEnumerator coroutine ) : this ( coroutine, true )
	{}

	public Job( IEnumerator coroutine, bool shouldStart )
	{
		_coroutine = coroutine;
		if ( shouldStart )
		{
			Start();
		}
	}

	public static Job Make(IEnumerator coroutine)
	{
		return new Job( coroutine );
	}

	public static Job Make(IEnumerator coroutine, bool shouldStart)
	{
		return new Job( coroutine, shouldStart);
	}

	public void Start()
	{
		_running = true;
		JobManager.instance.StartCoroutine( DoWork() );
	}

	public IEnumerator StartAsCoroutine()
	{
		_running = true;
		yield return JobManager.instance.StartCoroutine( DoWork());
	}

	public void Pause()
	{
		_paused = true;
	}

	public void UnPause()
	{
		_paused = false;
	}

	public void Kill()
	{
		_jobWasKilled = true;
		_running = false;
		_paused = false;
	}

	public void Kill( float delayInSeconds)
	{
		var delay = (int)( delayInSeconds * 1000);
		new System.Threading.Timer( obj => 
       	{	
			lock(this)
			{
				Kill();
			}
		}, null, delay, System.Threading.Timeout.Infinite );
	}

	private IEnumerator DoWork()
	{
		yield return null;

		while( _running)
		{
			if( _paused )
			{
				yield return null;
			}
			else
			{
				if( _coroutine.MoveNext() )
				{
					yield return _coroutine.Current;
				}
				else
				{
					_running = false;
				}
			}
		}

		if( jobComplete != null)
			jobComplete( _jobWasKilled );
	}


}