using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Spell
{
	public int id = 0;
	public string nameInLoc = "";
	public int level = 1;
	public List<Ingredient> ingredients = new List<Ingredient>();
	public float spellPowerModifier = 0;
	public int targets = 0;
	public SpellEffects effect = SpellEffects.direct;
}

[System.Serializable]
public class Ingredient
{
	public IngredientTypes type = IngredientTypes.flower;
	public int position = 0;
}

[System.Serializable]
public class CharacterParams
{
	public int hp;
	public int damage;
	public int healingPower;
	public int canMakeProtection;
	public CharacterType type;
}

[System.Serializable]
public class InventoryItemParams
{

	public InventoryItemType type;
	public int itemId;

	public int armorValue;
	public int dodgeValue;
	public int speedValue;
	

	public InventoryItemParams(InventoryItemParams par)
	{
		type = par.type;
		itemId = par.itemId;
		
		armorValue = par.armorValue;
		dodgeValue = par.dodgeValue;
		speedValue = par.speedValue;
	}

	public InventoryItemParams()
	{
		type = InventoryItemType.ARMOR;
		itemId = -1;
		
		armorValue = 0;
		dodgeValue = 0;
		speedValue = 0;
	}
}

[System.Serializable]
public class InventoryMenuSlot
{
	public InvetorySlotTypes type;
	public GameObject slotObject;
	public int itemId;
	public tk2dSprite sprite;
	public tk2dSlicedSprite frame;
	public tk2dTextMesh slotName;
}

[System.Serializable]
public class SkillParams
{
	public int id; //skill id
	public int lvl; // skill lvl
	public SkillTypes skillType;
	public EffectTypes effectType;
	// can be both active and passive
	public int radius; // 0 - whole map, >0 - count of tiles up/down/left/right, half of that amount for diagonal tiles
	public int targetsCount; // 1 - select 1 target in radius, >1 - random targets in radius, 0 = all in radius
	public int value; // uses for bufs/debufs
	public int duration;  //0 - always, >0 - count of rounds
	public int charges;
	public int manacost;
	public int expCost;

	public SkillParams(SkillParams par)
	{
		id = par.id; 
		lvl = par.lvl;
		skillType = par.skillType;
		effectType = par.effectType;
		radius = par.radius; 
		targetsCount = par.targetsCount; 
		value = par.value; 
		duration = par.duration;  
		charges = par.charges;
		manacost = par.manacost;
		expCost = par.expCost;
	}

	public SkillParams()
	{
		id = -1; 
		lvl = -1;
		skillType = SkillTypes.NONE;
		effectType = EffectTypes.NONE;
		radius = -1; 
		targetsCount = -1; 
		value = -1; 
		duration = -1; 
		charges = 0;
		manacost = 0;
		expCost = 0;
	}
}

[System.Serializable]
public class ImportedString
{
	public string caption = "empty";
	public string text = "empty";
}

[System.Serializable]
public class SkillUpgradeWay
{
	public tk2dSprite mainIcon;
	public tk2dSprite firstUpgrade;
	public tk2dSprite secondUpgrade;
	public tk2dSprite thirdUpgrade;
	public GameObject plusButton;
}

[System.Serializable]
public class CharacterGenerationParameters
{
	public int armorValue = 0;
	public int evasionValue = 0;
	public int speedValue = 0;
	public int healthPoints = 50;
	public int currentHealthPoint = 50;
	public int manaPoints = 50;
	public int currentManaPoint = 50;
	public List<int> skillsIds = new List<int>();

	public CharacterGenerationParameters(CharacterGenerationParameters par)
	{
		armorValue = par.armorValue;
		evasionValue = par.evasionValue;
		speedValue = par.speedValue;
		healthPoints = par.healthPoints;
		currentHealthPoint = par.currentHealthPoint;
		manaPoints = par.manaPoints;
		currentManaPoint = par.currentManaPoint;
		par.skillsIds.ForEach( t => skillsIds.Add(t) );
	}

	public CharacterGenerationParameters()
	{
		armorValue = 0;
		evasionValue = 0;
		speedValue = 0;
		healthPoints = 50;
		currentHealthPoint = 50;
		manaPoints = 50;
		currentManaPoint = 50;
		skillsIds = new List<int>();
	}
}

[System.Serializable]
public class StageInfo
{
	public int stageId = 0;
	public int enemiesCount = 10;
	public MainGoalsTypes mainGoalType = MainGoalsTypes.KILL_ALL;
	public SecondGoal secondGoal = new SecondGoal();
}

[System.Serializable]
public class Statistic
{
	public int exp = 0;
	public int moves = 0;
	public int kills = 0;
	public int longestCombo = 1;
	public int cleanKills = 0;
	public int fastKills = 0;
	public int frontKills = 0;
	public int hitsTaken = 0;
	public int secondGoalsCompleted = 0;
}

[System.Serializable]
public class SecondGoal
{
	public SecondGoalsTypes type;
	public int value;
}

[System.Serializable]
public class PlayerParameters
{
	public int health;
	public int mana;
	public int armor;
	public int evasion;
	public int speed;
	public List<int> skillsIds;
}