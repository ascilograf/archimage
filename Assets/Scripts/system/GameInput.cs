﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//функции инпута
public static class GameInput 
{


	private static Vector2 tapDown, tapUp;
	private static float tapTremble = 2;
	private static Camera guiCamera;
	private static Camera mainCamera;
	private static CameraStates state;

	public static void SetCameras(Camera gui, Camera main)
	{
		guiCamera = gui;
		mainCamera = main;
	}

	public static void SetCameraState(CameraStates inState)
	{
		state = inState;
	}

	//контроль нажатий по экрану, 
	//во всех случаях обработки луча выполняется следующий механизм:
	//вначале заапускается луч по GUI слою, и если было попадание, то происходит переход к следующему событию, либо рассылка
	//если же попадания в GUI не было, то происходит каст по дефолт слою,
	//если не было попаданий вовсе, то рассылается сообщение что палец просто был поднят.
	public static void Tap() 
	{
		GameObject caughtObject = null;
		if(SystemInfo.deviceType == DeviceType.Handheld)
		{
			if(Input.touchCount > 0)
			{
				//обработка нажатия, начало
				if(Input.GetTouch(0).phase == TouchPhase.Began)
				{
					tapDown = SetTapDown();
					
					if(CheckTapOnLayers() != null)
					{
						Messenger<GameObject>.Broadcast(Constants.startTapOnGUI, CheckTapOnLayers());
					}
				}
				
				//обработка нажатия, окончание
				if(Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled)
				{
					tapUp = SetTapUp();
					
					if(Vector3.Distance(tapUp, tapDown) > tapTremble)
					{
						return;
					}
					
					caughtObject = CheckTapOnLayers();
					
					tapUp = Vector2.zero;
					tapDown = Vector2.zero;

					if(caughtObject == null)
					{
						Debug.Log("null go");
						return;
					}
				}
			}
		}
		else
		{
			//обработка нажатия, начало
			if(Input.GetMouseButtonDown(0))
			{
				tapDown = SetTapDown();

				if(CheckTapOnLayers() != null)
				{
					Messenger<GameObject>.Broadcast(Constants.startTapOnGUI, CheckTapOnLayers());
				}
			}
			//обработка нажатия, окончание
			if(Input.GetMouseButtonUp(0))
			{
				tapUp = SetTapUp();
			
				if(Vector3.Distance(tapUp, tapDown) > tapTremble)
				{
					return;
				}
				
				caughtObject = CheckTapOnLayers();
				
				tapUp = Vector2.zero;
				tapDown = Vector2.zero;

				if(caughtObject == null)
				{
					return;
				}
			}
		}

		if(caughtObject != null )
		{
			Messenger<GameObject>.Broadcast(Constants.tapOnObjectEvent, caughtObject);
		}
	}

	public static void RightButtonClick()
	{
		GameObject caughtObject = null;
		//обработка нажатия, окончание
		if(Input.GetMouseButtonUp(1))
		{
			tapUp = SetTapUp();
			
			caughtObject = CheckTapOnLayers();
			
			tapUp = Vector2.zero;
			tapDown = Vector2.zero;
			//UnityEngine.Debug.Log(caughtObject.name);
			if(caughtObject == null)
			{
				return;
			}
		}
	
		if(caughtObject != null )
		{
			Messenger<GameObject>.Broadcast(Constants.rightClickOnObjectEvent, caughtObject);
		}
	}

	public static Vector2 SetTapDown()
	{
		Vector2 tapDown2 = new Vector2(10000,10000);
		if(SystemInfo.deviceType == DeviceType.Desktop || SystemInfo.deviceType == DeviceType.Unknown)
		{
			if(Input.GetMouseButtonDown(0))
			{
				tapDown2.x = Input.mousePosition.x;
				tapDown2.y = Input.mousePosition.y;
			}
		}
		else if(SystemInfo.deviceType == DeviceType.Handheld)
		{
			if(Input.touchCount > 0)
			{
				if(Input.GetTouch(0).phase == TouchPhase.Began)
				{
					tapDown2.x = Input.GetTouch(0).position.x;
					tapDown2.y = Input.GetTouch(0).position.y;
				}
			}
		}
		return tapDown2;
	}

	public static Vector2 SetTapUp()
	{
		Vector2 tapUp2 = new Vector2(-10000,-10000);
		if(SystemInfo.deviceType == DeviceType.Desktop || SystemInfo.deviceType == DeviceType.Unknown)
		{
			if(Input.GetMouseButtonUp(0))
			{
				tapUp2.x = Input.mousePosition.x;
				tapUp2.y = Input.mousePosition.y;
			}
		}
		else if(SystemInfo.deviceType == DeviceType.Handheld)
		{
			if(Input.touchCount > 0)
			{
				if(Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled)
				{
					tapUp2.x = Input.GetTouch(0).position.x;
					tapUp2.y = Input.GetTouch(0).position.y;
				}
			}
		}
		return tapUp2;
	}

	public static GameObject CheckTapOnLayers()
	{
		GameObject caughtObject = null;
		//int layer = 1 << 10;
		RaycastHit hit;
		int layer = 1 << 8;
		Ray ray = guiCamera.ScreenPointToRay(Input.mousePosition);

		if(Physics.Raycast(ray, out hit, Mathf.Infinity, layer))
		{
			caughtObject = hit.collider.gameObject;
		}
		else
		{
			if(state == CameraStates.menu)
				return null;
			
			layer = 1 << 0;
			ray = mainCamera.ScreenPointToRay(Input.mousePosition);
			if(Physics.Raycast(ray, out hit, Mathf.Infinity, layer))
			{
				caughtObject = hit.collider.gameObject;
			}
		}
		return caughtObject;
	}
}
