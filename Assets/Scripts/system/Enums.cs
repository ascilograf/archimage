﻿using UnityEngine;
using System.Collections;

public enum CameraStates
{
	stage = 0,
	menu = 1,
}

public enum Weapon
{
	grenade,
	bazooka,
	rifle,
	none,
}

public enum AimingType
{
	aiming,
	throwing,
	planting,
	charging,
}

public enum EnemyBehaviourType
{
	aggressive,
	standing,
	passive,
}

public enum IngredientTypes
{
	none = 0,
	flower = 1,
	skull = 2,
	mushroom = 3,
}

public enum SpellEffects
{
	direct = 1,
	diagonal = 2, 
	freeze = 3,
	self = 4,
}

public enum CharacterType
{
	none = 0,
	berserker = 1,
	golem = 2,
	healer = 3,
	monk = 4,
	enemyDD = 5,
	enemyHealer = 6,
}

public enum InventoryItemType
{
	ARMOR = 0,
	BOOTS = 1,
	AMULET = 2,
}

public enum NotificationTypes
{
	damage = 0,
	heal = 1,
	evasion = 2,
	parry = 3,
	block = 4,
	reflect = 5,
	vampiric = 6,
	critical = 7,
	miss = 8,
}

public enum InvetorySlotTypes
{
	BODY = 0,
	FEETS = 1,
	NECK = 2,
}

public enum Direction
{
	toLeft = 0,
	toRight = 1,
	toTop = 2,
	toBottom = 3,
}

public enum SkillTypes
{
	ACTIVE_BUFF,
	PASSIVE_BUFF,
	ACTIVE_DEBUFF,
	PASSIVE_DEBUFF,
	DAMAGE,
	BACKSTAB,
	BAIT,
	EARTHQUAKE,
	BLINK,
	DISORIENTATION,
	NONE,
}

public enum EffectTypes
{
	ARMOR,
	DODGE,
	SPEED,
	DAMAGE,
	NONE,
}

public enum MainGoalsTypes
{
	NONE = 0,
	KILL_ALL = 1,
}

public enum SecondGoalsTypes
{
	NONE = 0,
	CLEAN_KILLS = 1,
	FAST_KILLS = 2,
	FRONT_KILLS = 3,
	GET_LESS_HITS = 4,
}

public enum Rarity
{
	USUAL = 0,
	RARE = 1,
	UNIQUE = 2,
	EPIC = 3,
	LEGENDARY = 4,
}