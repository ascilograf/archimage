using UnityEngine;

//подгрузка текста для текстмеша из локкита
//поиск по названию переменной в списке импортированных текстов, при совпадении - грабит текст
//для нескольких значений заполняется массив значений по массиву имен переменных

public class GetTextFromFile : MonoBehaviour
{

    public string caption = "";

    public string addedAfterText;
    public string addedBeforeText;

    private tk2dTextMesh mesh = null;

    private void Start()
    {
		mesh = GetComponent<tk2dTextMesh>();
		//mesh.text = addedBeforeText + LineSlider.Utils.FindTextInLocKit(caption) + addedAfterText;
		//Messenger.AddListener(Constants.TextCommandConstants.RefreshGetTextFromFile, UpdateText);
    }

}