﻿using UnityEngine;
using System.Collections;
using CardGame;

public class Localization : MonoBehaviour 
{
	private Localization() {}

	private static Localization _instance;

	public static Localization getInstance
	{
		get{
			return _instance; }
	}

	public void Awake()
	{
		_instance = this;
	}

	public string GetCardDescriptionText(CardEffects forPlayer, CardEffects forEnemy)
	{
		string s = "";

		s += "^1";
		if(forPlayer.firstResValueChange != 0)
			s += "First res " + (forPlayer.firstResValueChange) + "\n";
		if(forPlayer.secondResValueChange != 0)
			s += "Second res " + (forPlayer.secondResValueChange) + "\n";
		if(forPlayer.thirdResValueChange != 0)
			s += "First res " + (forPlayer.thirdResValueChange) + "\n";
		
		if(forPlayer.firstIncrementChange != 0)
			s += "First res incr " + (forPlayer.firstIncrementChange) + "\n";
		if(forPlayer.secondIncrementChange != 0)
			s += "Second res incr " + (forPlayer.secondIncrementChange) + "\n";
		if(forPlayer.thirdIncrementChange != 0)
			s += "Third res incr " + (forPlayer.thirdIncrementChange) + "\n";
		
		if(forPlayer.healthChange != 0)
			s += "Health " + (forPlayer.healthChange) + "\n";
		if(forPlayer.shieldChange != 0)
			s += "Shield " + (forPlayer.shieldChange) + "\n";
		s += "^6";
		if(forEnemy.firstIncrementChange != 0)
			s += "First res " + (forEnemy.firstIncrementChange) + "\n";
		if(forEnemy.secondIncrementChange != 0)
			s += "Second res " + (forEnemy.secondIncrementChange) + "\n";
		if(forEnemy.thirdIncrementChange != 0)
			s += "First res " + (forEnemy.thirdIncrementChange) + "\n";
		
		if(forEnemy.firstResValueChange != 0)
			s += "First res incr " + (forEnemy.firstResValueChange) + "\n";
		if(forEnemy.secondResValueChange != 0)
			s += "Second res incr " + (forEnemy.secondResValueChange) + "\n";
		if(forEnemy.thirdResValueChange != 0)
			s += "Third res incr " + (forEnemy.thirdResValueChange) + "\n";
		
		if(forEnemy.healthChange != 0)
			s += "Health " + (forEnemy.healthChange) + "\n";
		if(forEnemy.shieldChange != 0)
			s += "Shield " + (forEnemy.shieldChange) + "\n";
		return s;
	}
}
