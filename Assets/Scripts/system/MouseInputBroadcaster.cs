﻿using UnityEngine;
using System.Collections;

public class MouseInputBroadcaster : MonoBehaviour 
{
	public Camera guiCamera;
	public Camera mainCamera;

	// Use this for initialization
	void Awake () 
	{
		GameInput.SetCameras(guiCamera, mainCamera);
	}
	
	// Update is called once per frame
	void Update () 
	{
		GameInput.Tap();
		GameInput.RightButtonClick();
	}
}
