﻿using UnityEngine;
using System.Collections;

public static class Constants
{
	public static string tapOnObjectEvent = "Tap on object";
	public static string rightClickOnObjectEvent = "Right click on object";
	public static string startTapOnGUI = "Tap on gui begin";
	public static string playerMakeStep = "Player makes step";
	public static string roundPassed = "Round passed";

	public static string refreshHands = "Refresh hands";



	public const float MOVE_SPEED = 1f;
	public const float WAIT_BEFORE_MOVE = 0.1f;
}
