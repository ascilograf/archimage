﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CardGame;

public class EnemyHandDisplay : MonoBehaviour 
{
	public List<CardDisplay> cards;
	// Use this for initialization
	public void StartGame () 
	{
		RefreshCards();
		StartCoroutine(StartAnimation());
	}
	
	
	public void RefreshCards()
	{
		for(int i = 0; i < cards.Count; i++)
		{
			cards[i].SetCard(GameController.getInstance.playerHand.cards[i], GameController.player);
		}
	}
	
	IEnumerator StartAnimation()
	{
		float timer = 0;
		float animTime = 0.5f;
		Vector3 startPos = new Vector3(0,0,0);
		while(true)
		{
			for(int i = 0; i < cards.Count; i++)
			{
				Vector3 finPos = new Vector3( - i * 100, 0, i * 10);
				cards[i].transform.localPosition = Vector3.Lerp(startPos, finPos, timer/animTime);
				cards[i].index = i;
			}
			
			if(timer > animTime)
			{
				yield break;
			}
			
			timer += Time.deltaTime;
			yield return null;
		}
	}

	void Update()
	{
		RefreshCards();
	}
	
	Vector3 positionDifference;
	Vector3 movingCardPosition;
	Vector3 startCardPosition;
	CardDisplay cardToMove;
}
