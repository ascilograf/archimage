﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CardGame;

public class PlayerHandDisplay : MonoBehaviour 
{
	public List<CardDisplay> cards;

	public CardDisplay tableCard;
	// Use this for initialization
	public void StartGame () 
	{
		RefreshCards();
		StartCoroutine(StartAnimation());
	}


	public void RefreshCards()
	{
		for(int i = 0; i < cards.Count; i++)
		{
			cards[i].SetCard(GameController.getInstance.playerHand.cards[i], GameController.player);
		}
	}

	IEnumerator StartAnimation()
	{
		float timer = 0;
		float animTime = 0.5f;
		Vector3 startPos = new Vector3(0,0,0);
		while(true)
		{
			for(int i = 0; i < cards.Count; i++)
			{
				Vector3 finPos = new Vector3(i * 100, 0, i * 10);
				cards[i].transform.localPosition = Vector3.Lerp(startPos, finPos, timer/animTime);
				cards[i].index = i;
			}

			if(timer > animTime)
			{
				StartCoroutine(PickingCard());
				yield break;
			}

			timer += Time.deltaTime;
			yield return null;
		}
	}

	IEnumerator RefreshAnimation()
	{
		float timer = 0;
		float animTime = 0.5f;
		List<Vector3> startPoses = new List<Vector3>();
		for(int i = 0; i < cards.Count; i++)
		{
			startPoses.Add(cards[i].transform.localPosition);
		}
		//
		while(true)
		{
			for(int i = 0; i < cards.Count; i++)
			{
				Vector3 finPos = new Vector3(i * 100, 0, i * 10);
				cards[i].transform.localPosition = Vector3.Lerp(startPoses[i], finPos, timer/animTime);
				cards[i].index = i;
			}
			
			if(timer > animTime)
			{
				cardToMove = null;
				StartCoroutine(PickingCard());
				yield break;
			}
			
			timer += Time.deltaTime;
			yield return null;
		}

	}

	Vector3 positionDifference;
	Vector3 movingCardPosition;
	Vector3 startCardPosition;
	CardDisplay cardToMove;
	
	IEnumerator PickingCard()
	{
		while(true)
		{
			if(Input.GetMouseButtonDown(0) && cardToMove == null)
			{
				if(cards.Exists(t => t.gameObject == GameInput.CheckTapOnLayers()))
				{
					cardToMove = GameInput.CheckTapOnLayers().GetComponent<CardDisplay>();
					startCardPosition = cardToMove.transform.position;
					positionDifference = cardToMove.transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
				}
			}
			if(Input.GetMouseButton(0) && cardToMove != null)
			{
				movingCardPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) + positionDifference;;
				movingCardPosition.z = -10;
				cardToMove.transform.localScale = Vector3.one * 1.2f;
				cardToMove.transform.position = movingCardPosition;
			}
			if(Input.GetMouseButtonUp(0) && cardToMove != null)
			{
				if(Input.mousePosition.y < 100)
				{
					GameController.getInstance.PlayerDiscardCard(cardToMove.index);
					startCardPosition.x = 1200;
					cardToMove.transform.position = startCardPosition;
					cardToMove.transform.localScale = Vector3.one;
					
					RefreshCards();
					StartCoroutine(Pause());
				}
				else if(Input.mousePosition.y >= 300)
				{
					cardToMove.transform.position = Vector3.zero;
					cardToMove.transform.localScale = Vector3.one;
					StartCoroutine(Pause());
				}
				yield break;
			}
			yield return 0;
		}
	}

	IEnumerator Pause()
	{
		if(GameController.getInstance.playerTurn && GameController.player.CanUseCard(GameController.getInstance.playerHand.GetCard(cardToMove.index).GetFirstResourcesPrice,
		                                                                             GameController.getInstance.playerHand.GetCard(cardToMove.index).GetSecondResourcesPrice,
		                                                                             GameController.getInstance.playerHand.GetCard(cardToMove.index).GetThirdResourcesPrice))
			GameController.getInstance.PlayerPickCard(cardToMove.index);
		/*else
		{
			cardToMove.transform.position = startCardPosition;
			cardToMove.transform.localScale = Vector3.one;
			yield break;
		}*/



		yield return new WaitForSeconds(1);

		startCardPosition.x = 1200;
		cardToMove.transform.position = startCardPosition;
		cardToMove.transform.localScale = Vector3.one;

		cards.Remove(cardToMove);
		cards.Add(cardToMove);

		RefreshCards();

		StartCoroutine(RefreshAnimation());
		yield break;
	}

}

