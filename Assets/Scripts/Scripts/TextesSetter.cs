﻿using UnityEngine;
using System.Collections;

public class TextesSetter : MonoBehaviour 
{
	public TextAsset cards;

	private TextesSetter() {}

	private static TextesSetter _instance;

	public static TextesSetter getInstance
	{
		get{	return _instance; }
	}

	void Awake()
	{
		_instance = this;
	}
}
