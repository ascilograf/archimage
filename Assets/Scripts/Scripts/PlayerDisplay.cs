﻿using UnityEngine;
using System.Collections;
using CardGame;

public class PlayerDisplay : MonoBehaviour 
{
	public tk2dTextMesh firstResourceValue;
	public tk2dTextMesh secondResourceValue;
	public tk2dTextMesh thirdResourceValue;

	public tk2dTextMesh firstResIncrement;
	public tk2dTextMesh secondResIncrement;
	public tk2dTextMesh thirdResIncrement;

	public tk2dTextMesh healthValue;
	public tk2dTextMesh shieldValue;

	void Update()
	{
		if(GameController.player == null)
			return;

		firstResourceValue.text = "Resource 1: " + GameController.player.GetFirstResValue();
		secondResourceValue.text = "Resource 2: " + GameController.player.GetSecondResValue().ToString();
		thirdResourceValue.text = "Resource 3: " + GameController.player.GetThirdResValue().ToString();
		firstResIncrement.text = "+" + GameController.player.GetFirstResValueIncrement().ToString();
		secondResIncrement.text = "+" + GameController.player.GetSecondResValueIncrement().ToString();
		thirdResIncrement.text = "+" + GameController.player.GetThirdResValueIncrement().ToString();
		healthValue.text = "Health: " + GameController.player.GetHealth().ToString();
		shieldValue.text = "Shield: " + GameController.player.GetShield().ToString();
	}
}
