﻿using UnityEngine;
using System.Collections;
using CardGame;

public class EnemyDisplay : MonoBehaviour 
{

	public tk2dTextMesh firstResourceValue;
	public tk2dTextMesh secondResourceValue;
	public tk2dTextMesh thirdResourceValue;
	
	public tk2dTextMesh firstResIncrement;
	public tk2dTextMesh secondResIncrement;
	public tk2dTextMesh thirdResIncrement;
	
	public tk2dTextMesh healthValue;
	public tk2dTextMesh shieldValue;
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	void Update()
	{
		if(GameController.enemy == null)
			return;

		firstResourceValue.text = "Resource 1: " + GameController.enemy.GetFirstResValue();
		secondResourceValue.text = "Resource 2: " + GameController.enemy.GetSecondResValue().ToString();
		thirdResourceValue.text = "Resource 3: " + GameController.enemy.GetThirdResValue().ToString();
		firstResIncrement.text = "+" + GameController.enemy.GetFirstResValueIncrement().ToString();
		secondResIncrement.text = "+" + GameController.enemy.GetSecondResValueIncrement().ToString();
		thirdResIncrement.text = "+" + GameController.enemy.GetThirdResValueIncrement().ToString();
		healthValue.text = "Health: " + GameController.enemy.GetHealth().ToString();
		shieldValue.text = "Shield: " + GameController.enemy.GetShield().ToString();
	}
}
