﻿using UnityEngine;
using System.Collections;
using CardGame;

public class CardDisplay : MonoBehaviour 
{
	public tk2dTextMesh descriptionCaption;
	public tk2dTextMesh priceCaption;
	public tk2dTextMesh nameCaption;
	public GameObject blocker;

	public int index;

	Card card;
	// Use this for initialization
	void Start () 
	{
		Messenger<GameObject>.AddListener(Constants.tapOnObjectEvent, CheckTap);
		Messenger<GameObject>.AddListener(Constants.rightClickOnObjectEvent, RightClick);
	}
	
	void CheckTap(GameObject target)
	{
		if(target == gameObject)
		{

		}
	}

	void RightClick(GameObject target)
	{
		/*if(target == gameObject)
		{
			GameController.getInstance.PlayerDiscardCard(index);
		}*/
	}

	public void SetCard(Card card, Player player)
	{
		this.card = card;

		if(card.GetFirstResourcesPrice > 0)
		{
			priceCaption.text = card.GetFirstResourcesPrice.ToString();
			priceCaption.color = Color.yellow;
		}
		else if(card.GetSecondResourcesPrice > 0)
		{
			priceCaption.text = card.GetSecondResourcesPrice.ToString();
			priceCaption.color = Color.blue;
		}
		else if(card.GetThirdResourcesPrice > 0)
		{
			priceCaption.text = card.GetThirdResourcesPrice.ToString();
			priceCaption.color = Color.green;
		}
		else if(card.GetThirdResourcesPrice == 0)
		{
			priceCaption.text = "0";
			priceCaption.color = Color.white;
		}

		if(!player.CanUseCard(card.GetFirstResourcesPrice, card.GetSecondResourcesPrice, card.GetThirdResourcesPrice))
			blocker.SetActive(true);
		else
			blocker.SetActive(false);

		nameCaption.text = card.cardName;
		descriptionCaption.text = Localization.getInstance.GetCardDescriptionText(this.card.effectsForPlayer, this.card.effectsForEnemy);
	}


}
