﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CardGame
{
    public class Hand : IHand
    {
        public List<Card> cards;

        public Hand(int cardsCount)
        {
            this.cards = new List<Card>();
            for (int i = 0; i < cardsCount; i++ )
                AddCard();
        }

        public void AddCard() 
        {
            //UnityEngine.Debug.Log("Card added to hand: " + card.GetName);
			cards.Add(CardDeck.getInstance.GetCard());
        }

        public void RemoveCard(int index)
        {
            UnityEngine.Debug.Log("Card removed from hand: " + cards[index].GetName);
			CardDeck.getInstance.AddCard(cards[index]);
            cards.RemoveAt(index);
        }

        public Card GetCard(int index)
        {
            return cards[index];
        }

        public List<Card> GetCardsList()
        {
			//UnityEngine.Debug.Log("-----------------");
            //cards.ForEach(t => UnityEngine.Debug.Log("Card in hand: " + t.GetName));
            //UnityEngine.Debug.Log("----------------");
            return cards;
        }
    }


}
