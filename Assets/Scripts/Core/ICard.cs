﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CardGame
{
    interface ICard
    {
        void Use(Player player);
    }
}
