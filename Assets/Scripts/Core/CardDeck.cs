﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace CardGame
{
    class CardDeck
    {
        private static List<Card> cards;

        private static CardDeck _instance;

        private CardDeck() { }

		private Random randomIndex = new Random();

        public static CardDeck getInstance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new CardDeck();
                    cards = new List<Card>();
                    string text = TextesSetter.getInstance.cards.text;
                    cards = FillCardsList(text);
                }
                return _instance;
            }
        }

        public void AddCard(Card card)
        {
            //UnityEngine.Debug.Log("-------------------------");
            //UnityEngine.Debug.Log("Card added to the deck: " + card.GetName);
            cards.Add(card);
        }

        public Card GetCard()
        {
			int val = randomIndex.Next(0, cards.Count);
			UnityEngine.Debug.Log(val.ToString());
            Card card = cards[val];

            //UnityEngine.Debug.Log("-------------------------");
            //UnityEngine.Debug.Log("Card removed from the deck: " + card.GetName);
			//UnityEngine.Debug.Log("-------------------------");
            cards.Remove(card);
            return card;
        }

		public List<Card> GetCardsList()
		{
			return cards;
		}

        static List<Card> FillCardsList(string text)
        {
            List<Card> cardsList = new List<Card>();

            string[] lines = text.Split( "\n"[ 0 ] );
            for (int i = 2; i < lines.Length; i++)
            {
                string[] columns = lines[i].Split(";"[0]);

                if (String.IsNullOrEmpty(columns[1]))
                    break;

                string name = columns[1];
                int firstPrice, secondPrice, thirdPrice, playerFirstGenerator, playerSecondGenerator, playerThirdGenerator,
                    enemyFirstGenerator, enemySecondGenerator, enemyThirdGenerator, playerFirstResource, playerSecondResource, playerThirdResource,
                    enemyFirstResource, enemySecondResource, enemyThirdResource, playerChangeHealth, playerChangeShield, enemyChangeHealth, enemyChangeShield, turnAgain, discardCard = 0;

                Int32.TryParse(columns[2], out firstPrice);
                Int32.TryParse(columns[3], out secondPrice);
                Int32.TryParse(columns[4], out thirdPrice);

                Int32.TryParse(columns[5], out playerFirstGenerator);
                Int32.TryParse(columns[6], out playerSecondGenerator);
                Int32.TryParse(columns[7], out playerThirdGenerator);

                Int32.TryParse(columns[8], out enemyFirstGenerator);
                Int32.TryParse(columns[9], out enemySecondGenerator);
                Int32.TryParse(columns[10], out enemyThirdGenerator);

                Int32.TryParse(columns[11], out playerFirstResource);
                Int32.TryParse(columns[12], out playerSecondResource);
                Int32.TryParse(columns[13], out playerThirdResource);

                Int32.TryParse(columns[14], out enemyFirstResource);
                Int32.TryParse(columns[15], out enemySecondResource);
                Int32.TryParse(columns[16], out enemyThirdResource);

                Int32.TryParse(columns[17], out playerChangeHealth);
                Int32.TryParse(columns[18], out playerChangeShield);

                Int32.TryParse(columns[19], out enemyChangeHealth);
                Int32.TryParse(columns[20], out enemyChangeShield);

                Int32.TryParse(columns[21], out turnAgain);
                Int32.TryParse(columns[22], out discardCard);

                Card card = new Card(name, 

                    firstPrice, 
                    secondPrice, 
                    thirdPrice,

                    turnAgain,
                    discardCard,

                    playerFirstGenerator,
                    playerSecondGenerator,
                    playerThirdGenerator,

                    enemyFirstGenerator,
                    enemySecondGenerator,
                    enemyThirdGenerator,

                    playerFirstResource,
                    playerSecondResource,
                    playerThirdResource,

                    enemyFirstResource,
                    enemySecondResource,
                    enemyThirdResource,

                    playerChangeHealth, 
                    playerChangeShield, 

                    enemyChangeHealth, 
                    enemyChangeShield);

               // UnityEngine.Debug.Log("Card added to the deck: " + card.GetName);
                cardsList.Add(card);
            }

            return cardsList;
        }
    }
}
