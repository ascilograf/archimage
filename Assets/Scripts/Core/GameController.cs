﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CardGame
{
    public class GameController
    {
        public static Player player { get; private set; }
        public static Player enemy { get; private set; }

        public Hand playerHand { get; private set; }
        public Hand enemyHand { get; private set; }

		public bool gameFinished { get; private set; }

		public bool playerTurn { get; private set; }

        private GameController() { }

        private static GameController _instance;
		private static Random rand = new Random();

        public static GameController getInstance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GameController();
                }
                return _instance;
            }
        }

        public void Initialize()
        {
            player = new Player();
            enemy = new Player();
            playerHand = new Hand(6);
            enemyHand = new Hand(6);
			playerTurn = true;
        }

        public void PlayerPickCard(int index)
        {

           if (!player.CanUseCard(	playerHand.GetCard(index).GetFirstResourcesPrice,
                                    playerHand.GetCard(index).GetSecondResourcesPrice,
                                    playerHand.GetCard(index).GetThirdResourcesPrice))
                return;

            player.PayForCard(	playerHand.GetCard(index).GetFirstResourcesPrice,
                                playerHand.GetCard(index).GetSecondResourcesPrice,
                                playerHand.GetCard(index).GetThirdResourcesPrice);

            player.UseCard(playerHand.GetCard(index).GetEffectsForPlayer);

            enemy.UseCard(playerHand.GetCard(index).GetEffectsForEnemy);

            playerHand.RemoveCard(index);
			playerHand.AddCard();

			playerTurn = false;

			if(CheckGameEnd())
			{
				UnityEngine.Application.LoadLevel(0);
				return;
			}

			EnemyPickCard();

        }

		public void PlayerDiscardCard(int index)
		{
			playerHand.RemoveCard(index);
			playerHand.AddCard();
			
			playerTurn = false;
			
			EnemyPickCard();
		}

        public void EnemyPickCard()
        {
			List<int> indexes = new List<int>();

			for(int i = 0; i < 6; i++)
			{
				if(enemy.CanUseCard(enemyHand.GetCard(i).GetFirstResourcesPrice,
				                    enemyHand.GetCard(i).GetSecondResourcesPrice,
				                    enemyHand.GetCard(i).GetThirdResourcesPrice))
				{
					indexes.Add(i);
				}
			}

			int index = 0;
			if(indexes.Count > 0)
			{
				index = indexes[rand.Next(0, indexes.Count)];

	            /*if (!enemy.CanUseCard(enemyHand.GetCard(index).GetFirstResourcesPrice,
	                                    enemyHand.GetCard(index).GetSecondResourcesPrice,
	                                    enemyHand.GetCard(index).GetThirdResourcesPrice))
	                return;*/

	            enemy.PayForCard(	enemyHand.GetCard(index).GetFirstResourcesPrice,
	                            	enemyHand.GetCard(index).GetSecondResourcesPrice,
	                            	enemyHand.GetCard(index).GetThirdResourcesPrice);
	            enemy.UseCard(enemyHand.GetCard(index).GetEffectsForPlayer);

	            player.UseCard(enemyHand.GetCard(index).GetEffectsForEnemy);
			}
			else
			{
				index = rand.Next(0, 6);
				UnityEngine.Debug.Log("Card discarded: " + enemyHand.GetCard(index) );

			}

            enemyHand.RemoveCard(index);
			enemyHand.AddCard();

			playerTurn = true;



			RoundEnded();
			Messenger.Broadcast(Constants.refreshHands);
			UnityEngine.Debug.Log("------------------------------");
        }

		private void RoundEnded()
		{
			enemy.IncrementResources();
			player.IncrementResources();
		}

		private bool CheckGameEnd()
		{
			if(player.GetHealth() <= 0 || enemy.GetHealth() >= 100)
			{
				return true;
			}
			else 
			{
				return false;
			}
		}

        private void DisplayPlayerParams()
        {
            UnityEngine.Debug.Log("--------- player params -----------");
            UnityEngine.Debug.Log("Health: " + GameController.player.GetHealth());
            UnityEngine.Debug.Log("Shield: " + GameController.player.GetShield());
            UnityEngine.Debug.Log("First res: " + GameController.player.GetFirstResValue());
            UnityEngine.Debug.Log("Second res: " + GameController.player.GetSecondResValue());
            UnityEngine.Debug.Log("Third res: " + GameController.player.GetThirdResValue());
            UnityEngine.Debug.Log("First res incerement: " + GameController.player.GetFirstResValueIncrement());
            UnityEngine.Debug.Log("Second res incerement: " + GameController.player.GetSecondResValueIncrement());
            UnityEngine.Debug.Log("Third res incerement: " + GameController.player.GetThirdResValueIncrement());
        }

        private void DisplayEnemyParams()
        {
            UnityEngine.Debug.Log("--------- enemy params -----------");
            UnityEngine.Debug.Log("Health: " + GameController.enemy.GetHealth());
            UnityEngine.Debug.Log("Shield: " + GameController.enemy.GetShield());
            UnityEngine.Debug.Log("First res: " + GameController.enemy.GetFirstResValue());
            UnityEngine.Debug.Log("Second res: " + GameController.enemy.GetSecondResValue());
            UnityEngine.Debug.Log("Third res: " + GameController.enemy.GetThirdResValue());
            UnityEngine.Debug.Log("First res incerement: " + GameController.enemy.GetFirstResValueIncrement());
            UnityEngine.Debug.Log("Second res incerement: " + GameController.enemy.GetSecondResValueIncrement());
            UnityEngine.Debug.Log("Third res incerement: " + GameController.enemy.GetThirdResValueIncrement());
        }
    }
}
