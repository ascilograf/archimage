﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CardGame
{
    interface IHand
    {
        void AddCard();
        void RemoveCard(int index);
        List<Card> GetCardsList();
        Card GetCard(int index);
    }
}
