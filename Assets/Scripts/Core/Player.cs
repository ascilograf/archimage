﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CardGame
{
    public class Player
    {
        private int health;
        private int shield;

        private int firstResourceValue;
        private int secondResourceValue;
        private int thirdResourceValue;

        private int firstResourceIncrement;
        private int secondResourceIncrement;
        private int thirdResourceIncrement;

        public Player()
        {
            health = 15;
            shield = 15;
            
            firstResourceValue = 15;
            firstResourceIncrement = 1;

            secondResourceValue = 15;
            secondResourceIncrement = 1;

            thirdResourceValue = 15;
            thirdResourceIncrement = 1;
        }

        void AddHealth(int value)
        {
            health += value;
			if(health < 0)
				;
			if(health > 100)
				;
        }

        public int GetHealth()
        {
            return health;
        }

        void AddShield(int value)
        {
            shield += value;
			if(shield > 100)
				shield = 100;
			if(shield < 0)
				shield = 0;
        }

        public int GetShield()
        {
            return shield;
        }

        void AddFirstResValue(int value)
        {
            firstResourceValue += value;
			if(firstResourceValue < 0)
				firstResourceValue = 0;
        }

        public int GetFirstResValue()
        {
            return firstResourceValue;
        }

        void AddSecondResValue(int value)
        {
            secondResourceValue += value;
			if(secondResourceValue < 0)
				secondResourceValue = 0;
        }

        public int GetSecondResValue()
        {
            return secondResourceValue;
        }

        void AddThirdResValue(int value)
        {
            thirdResourceValue += value;
			if(thirdResourceValue < 0)
				thirdResourceValue = 0;
        }

        public int GetThirdResValue()
        {
            return thirdResourceValue;
        }

        void AddFirstResIncrement(int value)
        {
            firstResourceIncrement += value;
			if(firstResourceIncrement < 1)
				firstResourceIncrement = 1;
        }

        public int GetFirstResValueIncrement()
        {
            return firstResourceIncrement;
        }

        void AddSecondResValueIncrement(int value)
        {
            secondResourceIncrement += value;
			if(secondResourceIncrement < 1)
				secondResourceIncrement = 1;
        }

        public int GetSecondResValueIncrement()
        {
            return secondResourceIncrement;
        }

        void AddThirdResValueIncrement(int value)
        {  
            thirdResourceIncrement += value;
			if(thirdResourceIncrement < 1)
				thirdResourceIncrement = 1;
        }

        public int GetThirdResValueIncrement()
        {
            return thirdResourceIncrement;
        }

        public bool CanUseCard(int priceFirstRes, int priceSecondRes, int priceThirdRes)
        {
            if (firstResourceValue >= priceFirstRes &&
                secondResourceValue >= priceSecondRes &&
                thirdResourceValue >= priceThirdRes)
                return true;

            return false;
        }

        public void UseCard(CardEffects effects)
        {
            this.AddFirstResIncrement(effects.firstIncrementChange);
            this.AddSecondResValueIncrement(effects.secondIncrementChange);
            this.AddThirdResValueIncrement(effects.thirdIncrementChange);

            this.AddFirstResValue(effects.firstResValueChange);
            this.AddSecondResValue(effects.secondResValueChange);
            this.AddThirdResValueIncrement(effects.thirdResValueChange);

            this.AddHealth(effects.healthChange);
            this.AddShield(effects.shieldChange);
            //card.PlayerUse();
        }

        public void PayForCard(int priceFirstRes, int priceSecondRes, int priceThirdRes)
        {
            firstResourceValue -= priceFirstRes;
            secondResourceValue -= priceSecondRes;
            thirdResourceValue -= priceThirdRes;
        }

		public void IncrementResources()
		{
			AddFirstResValue(GetFirstResValueIncrement());
			AddSecondResValue(GetSecondResValueIncrement());
			AddThirdResValue(GetThirdResValueIncrement());
		}
    }
}
