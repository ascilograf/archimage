﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CardGame
{
    public abstract class BaseCard
    {
        public int id { get; internal set; }
        public int firstResourcePrice { get; internal set; }
        public int secondResourcePrice { get; internal set; }
        public int thirdResourcePrice { get; internal set; }

        public abstract void Use(Player player);
       

    }
}
