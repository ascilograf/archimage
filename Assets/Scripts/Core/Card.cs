﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CardGame
{
    [System.Serializable]
    public class CardEffects
    {
        public int firstIncrementChange = 0;
        public int secondIncrementChange = 0;
        public int thirdIncrementChange = 0;

        public int firstResValueChange = 0;
        public int secondResValueChange = 0;
        public int thirdResValueChange = 0;

        public int healthChange = 0;
        public int shieldChange = 0;
    }

	[System.Serializable]
    public class Card 
    {
        public String cardName;
        private int firstResourcePrice;
        private int secondResourcePrice;
        private int thirdResourcePrice;

        private int turnAgain;
        private int discardCard;

		public CardEffects effectsForPlayer;
		public CardEffects effectsForEnemy;

        public Card(String cardName, 
            int firstResourcePrice,
            int secondResourcePrice, 
            int thirdResourcePrice,

            int turnAgain,
            int discardCard,

            int playerFirstIncrement, 
            int playerSecondIncrement, 
            int playerThirdIncrement,

            int enemyFirstIncrement, 
            int enemySecondIncrement, 
            int enemyThirdIncrement, 

            int playerFirstResource, 
            int playerSecondResource,
            int playerThirdResource,

            int enemyFirstResource,
            int enemySecondResource,
            int enemyThirdResource,

            int playerChangeHealth, 
            int playerChangeShield, 

            int enemyChangeHealth, 
            int enemyChangeShield)
        {
            this.cardName = cardName;

            this.firstResourcePrice = firstResourcePrice;
            this.secondResourcePrice = secondResourcePrice;
            this.thirdResourcePrice = thirdResourcePrice;

            this.turnAgain = turnAgain;
            this.discardCard = discardCard;

            effectsForPlayer = new CardEffects();
            effectsForEnemy = new CardEffects();

            //эффекты игроку
            this.effectsForPlayer.firstIncrementChange = playerFirstIncrement;
            this.effectsForPlayer.secondIncrementChange = playerSecondIncrement;
            this.effectsForPlayer.thirdIncrementChange = playerThirdIncrement;

            this.effectsForPlayer.firstResValueChange = playerFirstResource;
            this.effectsForPlayer.secondResValueChange = playerSecondResource;
            this.effectsForPlayer.thirdResValueChange = playerThirdResource;

            this.effectsForPlayer.healthChange = playerChangeHealth;
            this.effectsForPlayer.shieldChange = playerChangeShield;

            //эффекты противнику
            this.effectsForEnemy.firstIncrementChange = enemyFirstIncrement;
			this.effectsForEnemy.secondIncrementChange = enemySecondIncrement;
			this.effectsForEnemy.thirdIncrementChange = enemyThirdIncrement;

			this.effectsForEnemy.firstResValueChange = enemyFirstResource;
			this.effectsForEnemy.secondResValueChange = enemySecondResource;
			this.effectsForEnemy.thirdResValueChange = enemyThirdResource;

            this.effectsForEnemy.healthChange = enemyChangeHealth;
            this.effectsForEnemy.shieldChange = enemyChangeShield;
        }

       

        public String GetName
        {
            get { return cardName; }
        }

        public int GetFirstResourcesPrice
        {
            get { return firstResourcePrice; }
        }

        public int GetSecondResourcesPrice
        {
            get { return secondResourcePrice; }
        }

        public int GetThirdResourcesPrice
        {
            get { return thirdResourcePrice; }
        }

        public CardEffects GetEffectsForPlayer
        {
            get { return effectsForPlayer; }
        }

        public CardEffects GetEffectsForEnemy
        {
            get {    return effectsForEnemy; }
        }

        /*public void PlayerUse()
        {
            //эффекты карты на игрока
            

            //эффекты карты на врага
            GameController.Enemy.AddFirstResIncrement(effectsForEnemy.firstIncrementChange);
            GameController.Enemy.AddSecondResValueIncrement(effectsForEnemy.secondIncrementChange);
            GameController.Enemy.AddThirdResValueIncrement(effectsForEnemy.thirdIncrementChange);

            GameController.Enemy.AddFirstResValue(effectsForEnemy.firstResValueChange);
            GameController.Enemy.AddSecondResValue(effectsForEnemy.secondResValueChange);
            GameController.Enemy.AddThirdResValueIncrement(effectsForEnemy.thirdResValueChange);

            GameController.Enemy.AddHealth(effectsForEnemy.healthChange);
            GameController.Enemy.AddShield(effectsForEnemy.shieldChange);

            UnityEngine.Debug.Log("Card used: " + cardName);
        }*/
    }
}
