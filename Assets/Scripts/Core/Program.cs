﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


namespace CardGame
{
    class Program
    {
        static void Main(string[] args)
        {
            GameController.getInstance.Initialize();

            UnityEngine.Debug.Log("--------- hands -----------");
            GameController.getInstance.playerHand.GetCardsList();
            GameController.getInstance.playerHand.GetCardsList();

            GameController.getInstance.PlayerPickCard(2);
            GameController.getInstance.EnemyPickCard();

            UnityEngine.Debug.Log("--------- hands -----------");
            GameController.getInstance.playerHand.GetCardsList();
            GameController.getInstance.enemyHand.GetCardsList();

            Console.ReadLine();
        }


    }
}
